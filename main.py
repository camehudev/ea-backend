from fastapi import FastAPI, Request, requests, Form, status
from fastapi.encoders import jsonable_encoder
import dataBancos, classTabelas, json
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import classTabelas



app = FastAPI()

origins = ['http://localhost:5173', 'http://127.0.0.1:5173','http://127.0.0.1:54171','http://127.0.0.1:8000/api/livros/upload/' ]

app.add_middleware(
    CORSMiddleware,
    allow_origins= origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class Upload(BaseModel):
    nomeArquivo: str
    bancoUsado: str
    
class DelItem(BaseModel):
    item_del : int
    bancoItem: str

class ConsultarItem(BaseModel):
    bancoItem: str
    

@app.get('/')
async def root():
    return 'PAGINA INICIAL'


@app.post('/api/v1/upload')
# Terá 2 argumentos nome do arquivo e banco
async def uploadExcell(arqUload: Upload):         
    try:        
        dados = dataBancos.paramUpload(arqUload.nomeArquivo)  
        
        if arqUload.bancoUsado == 'nubank':
            def nubank():
                for i in range(len(dados)):  
                    yield classTabelas.Nubank.InserirLista(None,dados[i]['data'], dados[i]['descricao'], dados[i]['valor'])
    
            return nubank()
        
        elif arqUload.bancoUsado == 'next':
            def next():
                for i in range(len(dados)):
                    yield classTabelas.Next.InserirLista(None,dados[i]['data'], dados[i]['descricao'], dados[i]['valor'])
                
            return next()
        
        elif arqUload.bancoUsado == 'caixa':
            def caixa():
                for i in range(len(dados)):
                    yield classTabelas.CaixaEconomica.InserirLista(None,dados[i]['data'], dados[i]['descricao'], dados[i]['valor'])

            return caixa()
                           
        
    except:
            return status.HTTP_404_NOT_FOUND   
    
        
@app.get('/api/consultar/{itemBanco}')
async def consultar(itemBanco: str): 
    try:
       
        if (itemBanco == 'next'):
            bancoConsultar = classTabelas.Next.ConsultarLista()
            return bancoConsultar
        else:
            if (itemBanco == 'nubank'):
                bancoConsultar = classTabelas.Nubank.ConsultarLista()
                return bancoConsultar
            
            else:
                if (itemBanco == 'caixa'):
                    bancoConsultar = classTabelas.CaixaEconomica.ConsultarLista()
                    return bancoConsultar
                else:
                    return status.HTTP_404_NOT_FOUND
    except:
        return status.HTTP_404_NOT_FOUND   
    
       
@app.post('/api/delete')
async def deletarItemLista(item: DelItem): 
    try:
        if (item.bancoItem == 'next'):
            item_deletado = classTabelas.Next.DeletarList(item.item_del)
            return item_deletado 
        else:
            if (item.bancoItem == 'nunbak'):
                item_Id_deletado = classTabelas.Nubank.DeletarList(item.item_del)
                return item_Id_deletado
            
            else:
                if (item.bancoItem == 'nunbak'):
                    item_Id_deletado = classTabelas.CaixaEconomica.DeletarList(item.item_del)
                    return item_Id_deletado
                else:
                    return status.HTTP_404_NOT_FOUND
    except:
        return status.HTTP_404_NOT_FOUND     
    
@app.post('/login')
async def login():
     return 'Tela de Login'




    
  
    
  



