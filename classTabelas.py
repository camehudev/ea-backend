import sqlalchemyDB
from fastapi import status

class Usuarios:
    def __init__(self):
        self.firstName    
        self.lastName
        self.userName
        self.userSenha
        self.userEmail
        
    
    def InserirLista(self, firstName, lastName, userName, userSenha, userEmail):
        return { f'{firstName}', f'{lastName}', f'{userName}', f'{userSenha}', f'{userEmail}'}
     
    def ConsultarLista(self):
        print('Estou acessando o Metodo... Consultar')
    
    def DeletarList(self, idUsuario):
        print('Estou acessando o Metodo... Deletar')
        
        

class Nubank:
    def __init__(self):
        
        self.dtLancamento
        self.historico
        self.valorLancamento
        
    
    def InserirLista(self, dtLancamento, hstLancamento, vlrLancamento):
        xlsx= sqlalchemyDB.Nubank(idBanco=None, dataLancamento = dtLancamento, historico = hstLancamento, valorLancamento=f'{vlrLancamento}')
        sqlalchemyDB.session.add(xlsx)
        sqlalchemyDB.session.commit()
        return 'Itens salvos com sucesso'
        
     
    def ConsultarLista():
        try:
            query = sqlalchemyDB.session.query(sqlalchemyDB.Nubank).all()
            return query
        except:
            return status.HTTP_204_NO_CONTENT
    
    def DeletarList(idLancamento):
        try:
            itemDeletar = sqlalchemyDB.session.query(sqlalchemyDB.Nubank).filter(sqlalchemyDB.Nubank.idBanco == idLancamento).one()
            sqlalchemyDB.session.delete(itemDeletar)
            sqlalchemyDB.session.commit()
            return status.HTTP_201_CREATED   
        
        except:
            return status.HTTP_204_NO_CONTENT
        

class Next:
    def __init__(self, ):
        
        self.dtLancamento
        self.historico
        self.valorLancamento
        
    
    def InserirLista(self, dtLancamento, hstLancamento, vlrLancamento):
        xlsx= sqlalchemyDB.Next(idBanco=None, dataLancamento = dtLancamento, historico = hstLancamento, valorLancamento=f'{vlrLancamento}')
        sqlalchemyDB.session.add(xlsx)
        sqlalchemyDB.session.commit()
        return 'Itens salvos com sucesso'
        
     
    def ConsultarLista():
        try:
            query = sqlalchemyDB.session.query(sqlalchemyDB.Next).all()
            return query
        except:
            return status.HTTP_204_NO_CONTENT
    
    def DeletarList(idLancamento):
        try:
            itemDeletar = sqlalchemyDB.session.query(sqlalchemyDB.Next).filter(sqlalchemyDB.Next.idBanco == idLancamento).one()
            sqlalchemyDB.session.delete(itemDeletar)
            sqlalchemyDB.session.commit()
            return status.HTTP_201_CREATED   
        
        except:
            return status.HTTP_204_NO_CONTENT
        

class CaixaEconomica:
    def __init__(self):
        
        self.dtLancamento
        self.historico
        self.valorLancamento
        
    
    def InserirLista(self, dtLancamento, hstLancamento, vlrLancamento):
        xlsx= sqlalchemyDB.Caixa(idBanco=None, dataLancamento = dtLancamento, historico = hstLancamento, valorLancamento=f'{vlrLancamento}')
        sqlalchemyDB.session.add(xlsx)
        sqlalchemyDB.session.commit()
        return 'Itens salvos com sucesso'
       
     
    def ConsultarLista():
        try:
            query = sqlalchemyDB.session.query(sqlalchemyDB.Caixa).all()
            return query
        except:
            return status.HTTP_204_NO_CONTENT
    
    def DeletarList(self, idLancamento):
         itemDeletar = sqlalchemyDB.session.query(sqlalchemyDB.Caixa).filter(sqlalchemyDB.Caixa.idBanco == idLancamento).one()
         sqlalchemyDB.session.delete(itemDeletar)
         sqlalchemyDB.session.commit()
         
         return status.HTTP_201_CREATED   
       